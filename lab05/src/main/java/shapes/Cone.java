//Yejun Sim
//2137374

package shapes;

public class Cone implements Shape3d{
    private double height;
    private double radius; 

    public Cone(double height, double radius) {
        if (height <= 0 || radius <= 0) {
            throw new IllegalArgumentException();
        }
        this.height = height;
        this.radius = radius;
    }

    //this method calculates the volume of a cone and returns it.
    public double getVolume() {
        return Math.PI*(this.radius*this.radius)*(this.height/3);
    }

    //this methods calculates the area of a cone and returns it.
    public double getSurfaceArea() {
        return Math.PI*this.radius*(this.radius+(Math.sqrt((this.height*this.height)+(this.radius*this.radius))));
    }
}
