//Hai Hoang Do
//2237535
package shapes;

public class Sphere implements Shape3d{
    private double height;
    private double radius;

    public Sphere(double height, double radius)
    {
        if(height != radius*2)
        {
            throw new IllegalArgumentException();
        }
        if(height <= 0 || radius <= 0)
        {
            throw new IllegalArgumentException();
        }
        this.height = height;
        this.radius = radius;
    }

    //Should return sphere's volume
    public double getVolume()
    {
        return Math.PI*(this.radius*this.radius*this.radius);
    }

    //Should return sphere's surface area
    public double getSurfaceArea()
    {
        return 4*Math.PI*(this.radius*this.radius);
    }
}
