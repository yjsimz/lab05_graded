//Hai Hoang Do
//2237535
package shapes;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

public class ConeTest {
    @Test
    public void checkConstructor()
    {
        try
        {
            Cone cone = new Cone(-10, -5);
            fail("Can not create a cone object with this parameter");
        }   
        catch(IllegalArgumentException e)
        {
            System.out.println(e.getMessage());
        }     
    }

    @Test
    public void checkVolume()
    {
        Cone cone = new Cone(10, 5);
        assertEquals(Math.PI*(5*5)*(10.0/3), cone.getVolume(), 0.00001);
    }

    @Test
    public void checkSurfaceArea()
    {
        Cone cone = new Cone(10,5);
        assertEquals(Math.PI*5*(5+Math.sqrt((10.0*10.0)+(5.0*5.0))), cone.getSurfaceArea(), 0.00001);
    }
}
