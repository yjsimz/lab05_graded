//Yejun Sim
//2137374

package shapes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

public class SphereTest {

    @Test
    public void checkVolume() {
        Sphere sphere = new Sphere(10, 5);
        assertEquals(Math.PI*(5*5*5), sphere.getVolume(), 0.00001);
    }

    @Test   
    public void checkSurfaceArea() {
        Sphere sphere = new Sphere(10, 5);
        assertEquals(4*Math.PI*(5*5), sphere.getSurfaceArea(), 0.00001);
    }

    @Test
    public void checkConstructor() {
        try 
        {
            Sphere sphere = new Sphere(-10, -5);
            fail("Cannot use negative values for calculations.");
        }
        catch(IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }
}
